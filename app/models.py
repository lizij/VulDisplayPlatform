from flask_login import UserMixin
from . import db, login_manager
import hashlib

"""本部分包含数据库模型文件，如需修改数据库请相应修改本文件以对应数据库结构"""

class User(UserMixin, db.Model):
    __tablename__ = "user_info"
    username = db.Column(db.VARCHAR(50), primary_key=True)
    password = db.Column(db.VARCHAR(50))
    vendor = db.Column(db.VARCHAR(100))
    type = db.Column(db.VARCHAR(10))
    state = db.Column(db.VARCHAR(10))
    options = db.Column(db.JSON())
    email = db.Column(db.String(30))
    phone = db.Column(db.String(20))

    @property
    def password_origin(self):
        raise AttributeError("password is not readable")

    @password_origin.setter
    def password_origin(self, password):
        self.password = md5(password)

    def get_id(self):
        return self.username

    def verity_password(self, password):
        return md5(password) == self.password

    def is_admin_or_oem(self):
        return self.is_admin() or self.is_oem()

    def is_admin(self):
        return self.type == "admin"

    def is_oem(self):
        return self.type == "oem"

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)

def md5(str):
    hl = hashlib.md5()
    hl.update(str.encode(encoding="utf-8"))
    return hl.hexdigest()

class Vul(db.Model):
    __tablename__ = "vul_info"
    vid = db.Column(db.VARCHAR(50), primary_key=True)
    description = db.Column(db.VARCHAR(500))
    severity = db.Column(db.VARCHAR(10))
    type = db.Column(db.VARCHAR(50))
    pub_date = db.Column(db.TIMESTAMP())
    name = db.Column(db.VARCHAR(50))

class Device(db.Model):
    __tablename = "device_info"
    device_id = db.Column(db.VARCHAR(50), primary_key=True)
    abi = db.Column(db.VARCHAR(50))
    vendor = db.Column(db.VARCHAR(100))
    android_version = db.Column(db.VARCHAR(20))
    kernel = db.Column(db.VARCHAR(20))

class Task(db.Model):
    __tablename__ = "task_info"
    device_id = db.Column(db.VARCHAR(50), primary_key=True)
    vid = db.Column(db.VARCHAR(50), primary_key=True)
    tid = db.Column(db.VARCHAR(20))
    time = db.Column(db.TIMESTAMP())