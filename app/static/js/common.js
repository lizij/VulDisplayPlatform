// if data < 3 then hide bar label
function barLabelFilter(dataList) {
    $(dataList).each(function (index, item) {
        if (item < 3) {
            dataList[index] = {value: item, label: {normal: {show: false}}};
        }
    });
}

function getAllMarkPoint(dataList) {
    var markPoints = [];
    $(dataList).each(function (index, item) {
        var markPoint = {
            symbol: "circle",
            symbolSize: 25,
            value: item,
            xAxis: index,
            yAxis: item,
            label: {normal: {textStyle: {fontFamily: "Microsoft YaHei", color: "black", fontSize: 16, fontWeight: 1000}}}
        };
        markPoints.push(markPoint)
    });
    return markPoints;
}

function getAllRiskNum(list, lowList, midList, highList, seriousList) {
    var result = [];

    $(list).each(function (index, item) {
        var count = 0;
        count += lowList[index] ? lowList[index] : 0;
        count += midList[index] ? midList[index] : 0;
        count += highList[index] ? highList[index] : 0;
        count += seriousList[index] ? seriousList[index] : 0;
        result.push(count);
    });

    return result;
}

function fillZero(number) {
    if (number < 10) {
        number = "0" + number;
    }
    return number;
}

function setNowTime() {
    // 当前时间: 2017年05月10日 周五 12:25:49
    var date = new Date();
    var year = date.getFullYear();
    var month = fillZero(date.getMonth() + 1);
    var lastMonth = date.getMonth();
    var day = fillZero(date.getDate());
    var dayOfWeek = date.getDay();
    var hour = fillZero(date.getHours());
    var minute = fillZero(date.getMinutes());
    var second = fillZero(date.getSeconds());

    switch (dayOfWeek) {
        case 1:dayOfWeek = "周一";break;
        case 2:dayOfWeek = "周二";break;
        case 3:dayOfWeek = "周三";break;
        case 4:dayOfWeek = "周四";break;
        case 5:dayOfWeek = "周五";break;
        case 6:dayOfWeek = "周六";break;
        case 7:dayOfWeek = "周日";break;
        default:break;
    }

    var nowTime = "当前时间: ";
    nowTime += year + "年" + month + "月" + day + "日 ";
    nowTime +=  dayOfWeek + " ";
    nowTime += hour + ":" + minute + ":" + second;

    $("#now-time").text(nowTime);
    setLastMonth(year, lastMonth);
}

function setLastMonth (year, lastMonth) {
    if (lastMonth <= 0) {
        lastMonth = 12;
        year--;
    }
    $(".last-month").html(year + "年" + lastMonth + "月");
    if (--lastMonth <= 0) {
        lastMonth = 12;
    }
    $(".last-last-month").html(lastMonth);
}