function initDefaultPie(chartTitle, legendData, seriesData) {
    /**
     * 生成默认饼图option
     * @param {string} chartTitle - 标题
     * @param {array} legendData - 图例
     * @param {array} seriesData - 数据
     * @return {object} option 图表格式
     */
    if (legendData.length !== seriesData.length) {
        return null;
    }

    var pieData = [];
    for (var i = 0; i < legendData.length; i++) {
        pieData.push({value:seriesData[i], name:legendData[i]});
    }

    var option = {
        //设置标题
        title: {
            text: chartTitle,
            // x:"center",
            textStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#ffffff",
                fontWeight: 0
            }
        },
        // 设置网格
        grid: {
            top: '5%',
            left: '5%',
            right: '5%',
            bottom: '20%',
            containLabel: true
        },
        itemStyle: {
            normal: {
                // 阴影的大小
                shadowBlur: 200,
                // 阴影水平方向上的偏移
                shadowOffsetX: 0,
                // 阴影垂直方向上的偏移
                shadowOffsetY: 0,
                // 阴影颜色
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        },
        //设置图例
        legend: {
            data: legendData,
            bottom: "0",
            selectedMode: true,
            textStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#ffffff",
                fontSize: 12
            },
            show: legendData.length <= 6
        },
        // 设置颜色
        color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#ce0086", "#a900ce", "#002ece", "#008dce", "#00ce8d", "#55ce00"],
        // 设置提示
        tooltip : {
            trigger: 'item',
            formatter: "{b} : {c} ({d}%)"
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                // dataView : {show: true, readOnly: false},
                magicType : {
                    show: true,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            x: '25%',
                            width: '50%',
                            funnelAlign: 'left',
                            max: 1548
                        }
                    }
                },
                restore : {show: true},
                saveAsImage : {
                    backgroundColor:"auto",
                    show: true
                }
            }
        },
        calculable : true,
        series : [
            {
                // name: '访问来源',
                type:'pie',
                radius : '55%',
                data: pieData
            }
        ]
    };
    return option;
}

function initDefaultChart(chartMode, chartTitle, legendData, xAxisData, yAxisData) {
    /**
     * 生成直方图或折线图option
     * @param {string} chartMode - 图表类型，bar表示直方图，line表示折线图
     * @param {string} chartTitle - 标题
     * @param {array} legendData - 图例
     * @param {array} xAxisData - x轴数据
     * @param {array} yAxisData - y轴数据
     * @return {object} option 图表格式
     */
    if (yAxisData && xAxisData && legendData.length === yAxisData.length ) {
        var option = {
            //设置标题
            title: {
                text: chartTitle,
                // x:"center",
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#ffffff",
                    fontWeight: 0
                    // fontSize: 28.8
                }
            },
            //设置图例
            legend: {
                data: legendData,
                bottom: 0,
                selectedMode:false,
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#ffffff",
                    fontSize: 12
                }
            },
            // 设置网格
            grid: {
                top: '20%',
                left: '5%',
                right: '5%',
                bottom: '10%',
                containLabel: true
            },
            calculable : true,
            // 设置x轴及数据
            xAxis : [
                {
                    type : 'category',
                    data: xAxisData,
                    axisLabel: {
                        interval:0,
                        rotate:40,
                        textStyle: {
                            fontFamily: "Microsoft YaHei",
                            color: "#e7ebff",
                            fontSize: 12
                        }
                    }
                }
            ],
            // 设置y轴
            yAxis : [
                {
                    type : 'value',
                    axisLabel: {
                        textStyle: {
                            fontFamily: "Microsoft YaHei",
                            color: "#ffffff",
                            fontSize: 12
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: "rgba(255, 255, 255, 0.1)"
                        }
                    }
                }
            ],
            // 设置颜色
            color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#ce0086", "#a900ce", "#002ece", "#008dce", "#00ce64", "#31ce00"],
            tooltip : {
                trigger: 'axis'
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    // dataView : {show: true, readOnly: false},
                    magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                    restore : {show: true},
                    saveAsImage : {
                        backgroundColor:"auto",
                        show: true
                    }
                }
            }
        };

        // 设置数据
        var series = [];
        $(yAxisData).each(function (index, item) {
            var data = {
                // 设置使用哪个图例
                name: legendData[index],
                type: chartMode,
                // 设置数据是否可以堆叠
                stack: 'stack',
                data: item,
                barWidth: '20%',
                // 设置先后顺序
                z: chartMode == "bar" ? index + 1 : 1,
                // 设置数据标签
                label: {
                    normal: {
                        x:"center",
                        show: chartMode == "bar" ? index == yAxisData.length - 1 : true,
                        position: "top",
                        formatter: function (params) {
                            if (chartMode == "bar") {
                                if (index != yAxisData.length - 1) return 0;
                                var sum = 0;
                                for (var i = 0; i < legendData.length; i++) {
                                    sum += yAxisData[i][params.dataIndex];
                                }
                                return sum;
                            }
                            return item[params.dataIndex];
                        },
                        textStyle: {
                            fontFamily: "Microsoft YaHei",
                            color: "#ffffff",
                            // fontWeight: "bolder",
                            fontSize: 16
                        }
                    }
                }
            };
            series.push(data);
        });

        option.series = series;

        return option;
    } else {
        return null;
    }
}