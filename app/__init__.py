from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

import pymysql
pymysql.install_as_MySQLdb() # 将pymysql安装为MySQLdb，提供给sqlalchemy使用

from config import config

bootstrap = Bootstrap()

db = SQLAlchemy()

login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.login_view = "user.login" # 登录页面，未登录则跳转到该页面
login_manager.login_message = "请登录后再访问其他页面" # 未登录提示信息

def create_app(config_name):
    """
    生成flask app
    :param config_name:配置文件名
    :return: flask app实例
    """
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    bootstrap.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)

    # 注册main部分
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    # 注册user蓝图
    from .user import user as user_blueprint
    app.register_blueprint(user_blueprint)

    return app