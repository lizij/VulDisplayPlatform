from flask import Blueprint

# 生成蓝图，名称为main
main = Blueprint("main", __name__)

# 漏洞威胁等级图例名称
severity_legends = ["低危", "中危", "高危", "严重"]

# 安卓版本图例名称
android_version_legends = ["Android 4", "Android 5", "Android 6", "Android 7", "Android 8"]

from .. import db

# 必须在末尾导入，避免循环导入依赖
from . import index, report, errors