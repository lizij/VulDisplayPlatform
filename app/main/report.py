from flask import *
from flask_login import current_user
import traceback

from . import main, db

"""报告页面逻辑"""
@main.route("/report", methods=["GET"])
def report():
    """
    返回任务列表，每条任务附带对应的漏洞信息
    :return:
    """
    total_num = 0
    tasks = []
    limits = request.args["limits"].split("-") if "limits" in request.args else [0, 10]
    limits = [int(x) for x in limits]
    try:
        sql = """
        SELECT count(distinct(task_info.tid)) as num
        from task_info, device_info
        WHERE task_info.device_id = device_info.device_id and (device_info.vendor like '%%{}%%' or device_info.device_id like '%%{}%%')
        """.format(current_user.vendor, current_user.vendor)
        total_num = db.engine.execute(sql).first()["num"]

        sql = """
        SELECT task_info.tid, task_info.time, device_info.* 
        from task_info, device_info
        WHERE task_info.device_id = device_info.device_id and (device_info.vendor like '%%{}%%' or device_info.device_id like '%%{}%%')
        GROUP BY task_info.tid
        ORDER BY task_info.tid desc
        LIMIT {}, {}
        """.format(current_user.vendor, current_user.vendor, limits[0], limits[1])
        tasks = db.engine.execute(sql).fetchall()
        tasks = [dict(x) for x in tasks]

        for task in tasks:
            sql = """
            select vul_info.*
            from task_info, vul_info
            where task_info.vid = vul_info.vid and task_info.tid = '{}'
            """.format(task["tid"])
            task["vuls"] = db.engine.execute(sql)

    except:
        traceback.print_exc()
    return render_template("report.html", tasks = tasks, limits=limits, total_num = total_num)