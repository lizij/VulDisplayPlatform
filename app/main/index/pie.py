from flask import *
from pymysql.cursors import DictCursor
import json
import traceback

from . import *

"""饼图 x数据_图例_pie"""
@main.route("/vnum_vseverity_pie", methods=['GET'])
def vnum_vseverity_pie():
    """
    漏洞数量x 漏洞等级legend
    :return:
    """
    try:
        sql = """
                SELECT severity, count(*) as num
                FROM task_info,vul_info 
                WHERE task_info.vid = vul_info.vid 
                GROUP BY severity
                """
        rows = db.engine.execute(sql)

        res = {"legend":severity_legends, "x":[0] * len(severity_legends)}
        for row in rows:
            res["x"][severity_legends.index(row["severity"])] = int(row["num"])
        return json.dumps(res, ensure_ascii=False)
    except:
        traceback.print_exc()
    return ""

@main.route("/dnum_vnum_pie", methods=['GET'])
def dnum_vnum_pie():
    """
    终端数量x 数量分布legend
    :return:
    """
    try:
        level = [int(x) for x in json.loads(request.args.get("level"), encoding="utf8")]
        legend = ["漏洞数量<{}".format(level[0])]
        i = 0
        while i < len(level) - 1:
            legend.append("漏洞数量在{}-{}".format(level[i], level[i + 1]))
            i = i + 1
        legend.append("漏洞数量>{}".format(level[-1]))

        sql = 'SELECT count(*) as num from task_info group by device_id'
        rows = db.engine.execute(sql)
        res = {"legend": legend, "x":[0] * len(legend)}

        for row in rows:
            i = 0
            while i < len(level):
                if level[i] > int(row["num"]):
                    break
                i = i + 1
            res["x"][i] += 1

        return json.dumps(res, ensure_ascii=False)
    except:
        traceback.print_exc()
        return ""

@main.route("/vnum_vtype_pie", methods=["GET"])
def vnum_vtype_pie():
    """
    漏洞数量x 漏洞类型legend
    :return:
    """
    try:
        sql = """select vul_info.type, count(*) as num
            from task_info, vul_info
            where task_info.vid = vul_info.vid
            group by vul_info.type
            order by num desc"""
        rows = db.engine.execute(sql)

        res = {"legend":[], "x":[]}
        for row in rows:
            res["legend"].append(row["type"])
            res["x"].append(row["num"])

        return json.dumps(res, ensure_ascii=False)

    except:
        traceback.print_exc()
        return ""