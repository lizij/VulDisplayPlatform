from flask import *
import json
import traceback
from . import *

"""其他部分"""
@main.route("/totals", methods=["GET"])
def totals():
    """
    包括目前检测过的 设备总数量 漏洞总种类数 漏洞总数 严重漏洞总数
    :return:
    """
    try:
        sql = """SELECT count(DISTINCT(device_id)) AS total_devices 
            FROM device_info;"""
        total_devices = db.engine.execute(sql).first()["total_devices"]

        sql = """SELECT count(*) AS vid_num 
            FROM vul_info;"""
        total_vids = db.engine.execute(sql).first()["vid_num"]

        sql = """SELECT count(*) AS vul_num 
            FROM task_info;"""
        total_vulns = db.engine.execute(sql).first()["vul_num"]

        sql = """SELECT count(*) as svul_num
            FROM task_info, vul_info 
            WHERE task_info.vid = vul_info.vid AND vul_info.severity = '严重';"""
        serious_vuls = db.engine.execute(sql).first()["svul_num"]

        res = {"total-devices": total_devices, "total-vids": total_vids, "total-vuls": total_vulns, "serious-vuls": serious_vuls}
        return json.dumps(res, ensure_ascii=False)
    except:
        traceback.print_exc()
    return ""

@main.route("/vinfo_keywords_count", methods=["GET"])
def vinfo_keywords_count():
    """
    根据关键词统计
    :return:
    """
    try:
        sql = """select name, count(*) as num
            from task_info, vul_info 
            where task_info.vid = vul_info.vid 
            group by name"""
        rows = db.engine.execute(sql)

        keywords = request.args["keywords"].split("#")
        count = {k: 0 for k in keywords}

        for row in rows:
            name = row[0]
            num = row[1]
            for k in keywords:
                if k in name:
                    count[k] += num

        return json.dumps(count, ensure_ascii=False)
    except:
        traceback.print_exc()
        return ""