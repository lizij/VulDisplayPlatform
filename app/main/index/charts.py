import json
import traceback
from . import *

"""直方图/折线图部分 x轴_y轴_图例_chart"""

LAST_12_MONTH = """date_format(task_info.time, '%%Y-%%m') > date_format(date_sub(curdate(), INTERVAL 12 MONTH), '%%Y-%%m')"""
MONTH = """date_format(task_info.time, '%%Y-%%m')"""

@main.route("/month_vnum_vseverity_chart", methods=["GET"])
def month_vnum_vseverity_chart():
    """
    时间x 漏洞数量y 漏洞等级legend
    :return:
    """
    try:
        sql = """
            SELECT {} AS month, vul_info.severity, count(*) AS num
            FROM task_info, vul_info
            WHERE task_info.vid = vul_info.vid AND {}
            GROUP BY month, severity
            ORDER BY month;
            """.format(MONTH, LAST_12_MONTH)
        rows = db.engine.execute(sql)
        return generate_chart_data(rows, severity_legends)
    except:
        traceback.print_exc()
    return ""

@main.route("/month_vnum_android_chart", methods=["GET"])
def month_vnum_android_chart():
    """
    月份x 漏洞数量y 安卓版本legend
    :return:
    """
    try:
        sql = """
            select {} as month, concat('Android ', substr(device_info.android_version, 1, 1)) as version, count(*) as num
            from task_info, vul_info, device_info
            where task_info.vid = vul_info.vid and task_info.device_id = device_info.device_id and {}
            group by month, version
            order by month;
            """.format(MONTH, LAST_12_MONTH)
        rows = db.engine.execute(sql)
        return generate_chart_data(rows, legends=android_version_legends)
    except:
        traceback.print_exc()
    return ""

@main.route("/month_vnum_watching_vids_chart", methods=["GET"])
def month_vnum_watching_vids_chart():
    """
    月份x 漏洞数量y 关注漏洞legend
    :return:
    """
    try:
        sql = """
            SELECT {} AS month, vul_info.vid, count(*) AS num
            FROM task_info, vul_info
            WHERE task_info.vid = vul_info.vid AND {}
                  AND vul_info.vid in ({})
            GROUP BY month, vul_info.vid
            ORDER BY month;
            """.format(MONTH, LAST_12_MONTH, str(current_user.options["watching_vids"])[1:-1])
        rows = db.engine.execute(sql)
        return generate_chart_data(rows, current_user.options["watching_vids"])
    except:
        traceback.print_exc()
    return ""

@main.route("/android_vnum_vseverity_chart", methods=["GET"])
def android_vnum_vseverity_chart():
    """
    安卓版本x 漏洞数量y 漏洞等级legend
    :return:
    """
    try:
        sql = """
            SELECT concat('Android ', substr(device_info.android_version, 1, 1)) AS version, vul_info.severity, count(*) AS num
            FROM task_info, vul_info, device_info
            WHERE task_info.vid = vul_info.vid AND task_info.device_id = device_info.device_id
            GROUP BY version, vul_info.severity
            """
        rows = db.engine.execute(sql)
        return generate_chart_data(rows, legends=severity_legends)
    except:
        traceback.print_exc()
    return ""

@main.route("/kernel_vnum_vseverity_chart", methods=["GET"])
def kernel_vnum_vseverity_chart():
    """
    安卓版本x 漏洞数量y 漏洞等级legend
    :return:
    """
    try:
        sql = """
                SELECT device_info.kernel, vul_info.severity, count(*) AS num
                FROM task_info, vul_info, device_info
                WHERE task_info.vid = vul_info.vid AND task_info.device_id = device_info.device_id
                GROUP BY device_info.kernel, vul_info.severity
                        """
        rows = db.engine.execute(sql)
        return generate_chart_data(rows, legends=severity_legends)
    except:
        traceback.print_exc()
    return ""

@main.route("/vendor_vnum_vseverity_chart", methods=["GET"])
def vendor_vnum_vseverity_chart():
    """
    厂商x 漏洞数量y 漏洞等级legend
    :return:
    """
    try:
        mode = request.args.get("mode")
        vendor = current_user.vendor
        if mode == "main":
            # 主流厂商
            oems = ["小米", "华为", "三星", "金立", "魅族", "努比亚", "锤子", "联想", "vivo", "OPPO"]

            rows = []
            for oem in oems:
                sql = """
                select vul_info.severity, count(*) as num
                from task_info, vul_info, device_info
                where task_info.vid = vul_info.vid and task_info.device_id = device_info.device_id and (device_info.vendor like '%%{}%%' or device_info.device_id like '%%{}%%')
                group by vul_info.severity
                """.format(oem, oem)
                rs = db.engine.execute(sql)
                for r in rs:
                    rows.append((oem, r[0], r[1]))
        else:
            # 前十名
            sql = """
                SELECT if(isnull(device_info.vendor) || length(device_info.vendor) < 1, '其他', device_info.vendor) AS vendor, vul_info.severity, count(*) AS num
                FROM task_info, vul_info, device_info
                WHERE task_info.vid = vul_info.vid AND task_info.device_id = device_info.device_id AND device_info.vendor IN (
                    SELECT A.vendor FROM (
                        SELECT device_info.vendor, count(*) AS num
                        FROM task_info, device_info
                        WHERE task_info.device_id = device_info.device_id
                        GROUP BY task_info.device_id
                        ORDER BY num DESC
                        LIMIT 10
                    ) AS A
                )
                GROUP BY device_info.vendor, vul_info.severity;
                """

            rows = db.engine.execute(sql)
        if vendor == "":
            return generate_chart_data(rows, legends=severity_legends)
        else:
            return generate_chart_data(rows, legends=severity_legends, anonymous=True)
    except:
        traceback.print_exc()
    return ""

def generate_chart_data(rows, legends, anonymous=False):
    """
    自动生成直方图/折线数据
    :param rows: 每一行必须只包含3个数据，分别对应x，legend和y
    :param legends: 使用的legends名称
    :param anonymous: 是否要进行匿名
    :return: dict，对应x，y，label 3个数组，x为x轴数据，y中每一个数组都对应一个legend和x轴的数据，label为该列显示的名字
    """

    res = {"x": [],
           "y": [[] for i in range(0, len(legends))],
           # "label": [],
           "legend": legends}
    cur = -1
    for row in rows:
        x_data = row[0]
        legend = row[1]
        count = int(row[2])
        if not x_data in res["x"]:
            res["x"].append(x_data)
            cur += 1
            for y in res["y"]:
                y.append(0)
            # res["label"].append(0)
        res["y"][legends.index(legend)][cur] += count
        # res["label"][cur] += count
    if anonymous:
        do_anonymous(res['x'])

    return json.dumps(res, ensure_ascii=False)

def do_anonymous(arr):
    """
    将数组中数据用A-Z替换
    :param arr:
    :return:
    """
    if len(arr) < 26:
        for i in range(0, len(arr)):
            arr[i] = chr(i + ord('A'))