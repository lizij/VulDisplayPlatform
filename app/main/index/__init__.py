from flask import *
from flask_login import login_required, current_user
from .. import *

"""检查是否登录"""
@main.before_request
@login_required
def login_check():
    """
    已登录的url不需要重定向到登录界面
    :return:
    """
    pass

"""主页部分逻辑"""
@main.route("/", methods=["GET"])
def index():
    """
    主页
    :return:
    """
    if current_user.is_authenticated:
        return render_template("index.html")
    else:
        return redirect(url_for("user.login"))

from . import charts, pie, rank, others