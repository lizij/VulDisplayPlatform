import json
import traceback

from . import *

"""排行部分 排行内容_排序关键字_rank"""
@main.route("/dinfo_vnum_rank", methods=['GET'])
def dinfo_vnum_rank():
    """
    设备风险排行
    设备ID 厂商名 漏洞数量
    :return:
    """
    try:
        vendor = current_user.vendor
        sql = """SELECT task_info.device_id, if(isnull(device_info.vendor) || length(device_info.vendor) < 1, '其他', device_info.vendor) AS vendor, count(*) AS num
            FROM task_info,device_info 
            WHERE task_info.device_id = device_info.device_id and (device_info.vendor like '%%{}%%' or device_info.device_id like '%%{}%%')
            GROUP BY device_id
            ORDER BY num DESC
            LIMIT 10""".format(vendor, vendor)
        rows = db.engine.execute(sql)
        res = {"product":[], "vendor":[], "vuls":[]}

        for row in rows:
            res["product"].append(row["device_id"])
            res["vendor"].append(row["vendor"])
            res["vuls"].append(row["num"])

        return json.dumps(res, ensure_ascii=False)
    except:
        traceback.print_exc()
        return ""

@main.route("/vinfo_vnum_rank", methods=["GET"])
def vinfo_vnum_rank():
    """
    漏洞数量排行
    漏洞名 漏洞数量
    :return:
    """
    try:
        sql = """select task_info.vid, count(*) as num
            from task_info, vul_info
            where task_info.vid = vul_info.vid
            group by task_info.vid
            order by num desc
            limit 10"""
        rows = db.engine.execute(sql)

        res = {"vul":[], "count":[]}

        for row in rows:
            res["vul"].append(row["vid"])
            res["count"].append(row["num"])

        return json.dumps(res, ensure_ascii=False)
    except:
        traceback.print_exc()
        return ""