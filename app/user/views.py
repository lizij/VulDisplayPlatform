from flask import *
from flask_login import login_user, logout_user, current_user, login_required
from . import user
from ..models import *
from .forms import LoginForm, SigninForm, EditProfileForm
from .. import db
import json
import traceback

@user.route("/login", methods=["GET", "POST"])
def login():
    """
    登录逻辑
    :return:
    """
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = User.query.filter_by(username=form.username.data).first()
            if user is not None and user.verity_password(form.password.data):
                if user.state == "active":
                    login_user(user)
                    return redirect(url_for("main.index"))
                else:
                    flash("用户尚未激活，请及时联系管理员进行激活")
        except:
            traceback.print_exc()
            flash("无效的用户名或密码")
    return render_template("login.html", form=form)

@user.route("/logout")
def logout():
    """
    注销逻辑
    :return:
    """
    logout_user()
    return redirect(url_for("user.login"))

@user.route("/signin", methods=["GET", "POST"])
def signin():
    """
    注册逻辑
    :return:
    """
    form = SigninForm()
    if form.validate_on_submit():
        try:
            user = User(username=form.username.data,
                        password_origin=form.username.data,
                        vendor=form.vendor.data,
                        type="oem",
                        state="inactive",
                        options={},
                        email=form.email.data,
                        phone=form.phone.data)
            db.session.add(user)
            flash("注册成功，请联系管理员激活账户，默认密码为账户名")
            return redirect(url_for("user.login"))
        except:
            traceback.print_exc()
            flash("注册失败，账户已存在或输入含有非法字符")

    return render_template("signin.html", form=form)

@user.route("/profile/<username>", methods=["GET", "POST"])
@login_required
def profile(username):
    form = EditProfileForm()
    user = None
    try:
        user = User.query.filter_by(username=username).first()
        if form.validate_on_submit():
            user.vendor = form.vendor.data
            user.email = form.email.data
            user.phone = form.phone.data
            # user.options = json.loads(form.options.data)
            db.session.add(user)
            flash("用户资料已经修改")
            return redirect(url_for("user.manage"))
    except:
        traceback.print_exc()
        flash("用户不存在")
    return render_template("profile.html", user=user, form=form)

@user.route("/manage", methods=["GET", "POST"])
@login_required
def manage():
    """
    用户管理界面，渲染界面，修改密码等功能
    :return:
    """
    users = []
    vuls = []
    if request.method == "GET":
        try:
            if current_user.type == "admin":
                users = User.query.all()
                vuls = Vul.query.all()
        except:
            traceback.print_exc()

        return render_template("manage.html", users=users, vuls=vuls)

    else:
        try:
            mode = request.form["mode"]

            if mode == "change_password":
                # 修改密码
                current_user.password_origin = request.form["data"]
                db.session.add(current_user)
                return "OK"

            elif mode == "change_email":
                # 修改邮箱
                current_user.email = request.form["data"]
                db.session.add(current_user)
                return "OK"

            elif mode == "change_phone":
                # 修改手机号
                current_user.phone = request.form["data"]
                db.session.add(current_user)
                return "OK"

            elif mode == "change_vendor":
                # 修改厂商信息
                if current_user.type == "oem":
                    # 厂商修改，修改成功则冻结账户
                    current_user.vendor = request.form["new_vendor"]
                    current_user.state = "inactive"
                    db.session.add(current_user)
                    return "OK"

                elif session["type"] == "admin":
                    # 管理员修改
                    user = User.query.filter_by(username=request.form["username"]).first()
                    user.vendor = request.form["data"]
                    db.session.add(user)
                    return "OK"

            elif mode == "reset" and current_user.type == "admin":
                # 重置密码
                user = User.query.filter_by(username=request.form["username"]).first()
                user.password_origin = user.username
                db.session.add(user)
                return "OK"

            elif mode == "active" and current_user.type == "admin":
                # 激活账户
                user = User.query.filter_by(username=request.form["username"]).first()
                user.state = "active"
                db.session.add(user)
                return "OK"

            elif mode == "freeze" and current_user.type == "admin":
                # 冻结账户
                user = User.query.filter_by(username=request.form["username"]).first()
                user.state = "inactive"
                db.session.add(user)
                return "OK"

            elif mode == "change_watching_vids" and current_user.type == "admin":
                # 修改关注漏洞

                watching_vids = request.form["data"]
                username = request.form["username"]
                sql = """update user_info 
                        set options = json_set(options, '$.watching_vids', json_array({}))
                        where username in ({})
                    """.format(watching_vids[1:-1], username[1:-1])
                db.engine.execute(sql)

                current_user.options["watching_vids"] = json.loads(watching_vids, encoding="utf8")
                db.session.add(current_user)
                return "OK"

        except:
            traceback.print_exc()
        return ""
