from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length, Email

class LoginForm(FlaskForm):
    username = StringField("用户名", validators=[DataRequired()])
    password = PasswordField("密码", validators=[DataRequired()])
    submit = SubmitField("登录")

class SigninForm(FlaskForm):
    username = StringField("用户名", validators=[DataRequired(), Length(1, 10, message="长度必须在1-10之间")])
    vendor = StringField("厂商名", validators=[DataRequired(), Length(1, 20, message="长度必须在1-20之间")])
    phone = StringField("手机号", validators=[DataRequired(), Length(5, 20, message="长度必须在5-20之间")])
    email = StringField("邮箱", validators=[DataRequired(), Email(message="邮箱格式不正确")])
    submit = SubmitField("注册")

class EditProfileForm(FlaskForm):
    vendor = StringField("厂商名")
    phone = StringField("手机号", validators=[DataRequired(), Length(5, 20, message="长度必须在5-20之间")])
    email = StringField("邮箱", validators=[DataRequired(), Email(message="邮箱格式不正确")])
    # options = TextAreaField("用户选项", validators=[DataRequired()])
    submit = SubmitField("修改")