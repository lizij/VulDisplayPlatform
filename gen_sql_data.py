import tdc
import pymysql

# 连接数据库
conn = pymysql.connect(host="localhost",
                       user="root",
                       password="root",
                       db="vuldisplay",
                       port=3306,
                       charset="utf8")

def main(root_path):
    ti_map = tdc.collect(root_path).ti_map # 任务数据
    vuln_map = tdc.VULN_MAP # 漏洞数据

    try:
        cursor = conn.cursor()

        # 逐行插入漏洞数据
        for vid in vuln_map:
            vuln = vuln_map[vid]
            sql = 'REPLACE INTO vul_info (vid, description, severity, `name`, type, pub_date) VALUES ("{}", "{}", "{}", "{}", "{}", "{}");'\
                .format(vid, vuln["desc"], vuln["severity"], vuln["name"], vuln["type"], vuln["pub_date"] if "pub_date" in vuln else "")
            cursor.execute(sql)

        # 逐行插入设备数据和任务数据
        for tid in ti_map:
            ti = ti_map[tid]
            sql = 'REPLACE INTO device_info (device_id, abi, vendor, android_version, kernel) VALUES ("{}", "{}", "{}", "{}", "{}");'\
                .format(ti.dID, ti.abi, ti.vID if ti.vID else "", ti.system, ti.kernel)
            cursor.execute(sql)

            for vid in ti.v_id:
                if ti.v_id[vid]:
                    sql = 'REPLACE INTO task_info (device_id, vid, tid, time) VALUES ("{}", "{}", "{}", "{}");'\
                        .format(ti.dID, vid, tid, ti.date[0:ti.date.index(',')])
                    cursor.execute(sql)

        conn.commit()
        print("数据插入完成")
    except Exception as e:
        print(e)
    finally:
        conn.close()


if __name__ == '__main__':
    main("test_data")