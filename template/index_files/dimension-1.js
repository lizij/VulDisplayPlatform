$(function(){
	setNowTime();
	// setInterval(setNowTime, 1000);
})

function fillZero(number) {
	if (number < 10) {
		number = "0" + number;
	}
	return number;
}
function setNowTime() {
	// 当前时间: 2017年05月10日 周五 12:25:49
	var date = new Date();
	var year = date.getFullYear();
	var month = fillZero(date.getMonth() + 1);
	var lastMonth = date.getMonth();
	var day = fillZero(date.getDate());
	var dayOfWeek = date.getDay();
	var hour = fillZero(date.getHours());
	var minute = fillZero(date.getMinutes());
	var second = fillZero(date.getSeconds());

	if (dayOfWeek == 1) {
		dayOfWeek = "周一";
	} else if (dayOfWeek == 2) {
		dayOfWeek = "周二";
	} else if (dayOfWeek == 3) {
		dayOfWeek = "周三";
	} else if (dayOfWeek == 4) {
		dayOfWeek = "周四";
	} else if (dayOfWeek == 5) {
		dayOfWeek = "周五";
	} else if (dayOfWeek == 6) {
		dayOfWeek = "周六";
	} else if (dayOfWeek == 7) {
		dayOfWeek = "周日";
	}

	var nowTime = "当前时间: ";
	nowTime += year + "年" + month + "月" + day + "日 ";
	nowTime +=  dayOfWeek + " "
	nowTime += hour + ":" + minute + ":" + second;

	$("#now-time").text(nowTime);
	setLastMonth(year, lastMonth);
}

function setLastMonth (year, lastMonth) {
    if (lastMonth <= 0) {
        lastMonth = 12;
        year--;
    }
    $(".last-month").html(year + "年" + lastMonth + "月");
    if (--lastMonth <= 0) {
        lastMonth = 12;
    }
    $(".last-last-month").html(lastMonth);
}

// chart
var safetyTrendOption = {
    legend: {
        data:['产品安全指数月平均值'],
        bottom: 40,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#A2A6B4",
            fontSize: 12
        }
    },
    grid: {
        top: '10%',
        left: '5%',
        right: '5%',
        bottom: '20%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['2017年2月', '2017年3月', '2017年4月', '2017年5月'],
            data: safetyTrendDateList,
            axisLabel: {
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
                }
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            max: maxY,
            min: minY,
            interval: 5,
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
                }
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#639DFE"],
    series : [
        {
            name:'产品安全指数月平均值',
            type:'line',
            // data:[60, 50, 40, 50, 60, 70, 60, 50],
            data: safetyTrendScoreList,
            symbol: "circle",
            symbolSize: 8,
            hoverAnimation: false,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
            // markPoint: {data: safetyMarkPoints}
        }
    ]
};
var safetyTrendChart = echarts.init($("#safety-trend-chart")[0]);
safetyTrendChart.setOption(safetyTrendOption);


var unfixedTrendOption = {
	// title: {
	// 	text: "未修复漏洞数月平均变化趋势",
	// 	textAlign: "center",
	// 	left: "50%",
	// 	textStyle: {
     //        fontFamily: "Microsoft YaHei",
	// 		color: "#00CCFF",
     //        fontWeight: 0,
     //        fontSize: 28.8
	// 	}
	// },
    legend: {
        data:['严重漏洞','高危漏洞','中危漏洞','低危漏洞'],
        bottom: 40,
        selectedMode: false,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#A2A6B4",
            fontSize: 12
        }
    },
    grid: {
        top: '10%',
        left: '5%',
        right: '5%',
        bottom: '20%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['2017年2月', '2017年3月', '2017年4月', '2017年5月'],
            data: vulTrendDateList,
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
            		color: "#A2A6B4",
                    fontSize: 12
            	}
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
            	}
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30"],
    series : [
        {
            name:'低危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 20, 25, 27, 32],
            data: vulTrendLowRiskNumAvgList,
            barWidth: '30%',
            z: 1,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    formatter: function (params) {
                        return avgCountList[params.dataIndex];
                    },
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
        },
        {
            name:'中危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 20, 25, 27, 32],
            data: vulTrendMidRiskNumAvgList,
            barWidth: '30%',
            z: 2,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    formatter: function (params) {
                        return avgCountList[params.dataIndex];
                    },
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
        },
        {
            name:'高危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 20, 25, 27, 32],
            data: vulTrendHighRiskNumAvgList,
            barWidth: '30%',
            z: 3,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    formatter: function (params) {
                        return avgCountList[params.dataIndex];
                    },
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
        },
        {
            name:'严重漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[8, 10, 7, 12, 8, 10, 7, 12],
            data: vulTrendSeriousRiskNumAvgList,
            barWidth: '30%',
            z: 4,
            label: {
            	normal: {
            		show: true,
                    position: "top",
                    formatter: function (params) {
                        return avgCountList[params.dataIndex];
                    },
            		textStyle: {
                        fontFamily: "Microsoft YaHei",
            			color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
            		}
            	}
            },
        }
    ]
};
var unfixedTrendChart = echarts.init($("#unfixed-trend-chart")[0]);
unfixedTrendChart.setOption(unfixedTrendOption);