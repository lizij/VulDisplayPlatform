// chart
var phoneHighOption = {
    title: {
        text: "旗舰产品按厂商统计",
        textAlign: "center",
        top: 20,
        left: "50%",
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "white",
            fontWeight: 0,
            fontSize: 30
        }
    },
    legend: {
        data:['严重漏洞','高危漏洞','中危漏洞','低危漏洞','产品安全指数'],
        bottom: 20,
        selectedMode: false,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#AAAEBB",
            fontSize: 16
        }
    },
    grid: {
    	top: '20%',
        left: '5%',
        right: '5%',
        bottom: '10%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['OEM1', 'OEM2', 'OEM3', 'OEM4', 'OEM5', 'OEM6', 'OEM7', 'OEM8', 'OEM9', 'OEM10', '其他'],
            data: phoneHighOrderNameList,
            axisLabel: {
                margin: 15,
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            name: "未修复漏洞数",
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#AAAEBB",
                fontSize: 16
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            },
            splitLine: {
                show: false
            }
        },
        {
        	type : 'value',
            name: "产品安全指数",
            min: 0,
            max: 100,
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#AAAEBB",
                fontSize: 16
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#639DFE"],
    series : [
        {
            name:'低危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneHighLowRiskNumList,
            barWidth: 25
        },
        {
            name:'中危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneHighMidRiskNumList,
            barWidth: 25
        },
        {
            name:'高危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneHighHighRiskNumList,
            barWidth: 25
        },
        {
            name:'严重漏洞',
            type:'bar',
            stack: '漏洞',
            // data: [8, 10, 7, 12, 10, 10, 8, 10, 7, 12, 10],
            data: phoneHighSeriousRiskNumList,
            barWidth: 25
        },
        {
            name:'产品安全指数',
            type:'line',
            yAxisIndex: 1,
            // data: [70, 60, 50, 45, 40, 40, 32, 44, 50, 47, 60]
            data: phoneHighSafetyScoreList,
            symbol: "circle",
            symbolSize: 8,
            hoverAnimation: false,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
            // markPoint: {data: phoneHighMarkPoints}
        }
    ]
};
var phoneHighChart = echarts.init($("#phone-high-chart")[0]);
phoneHighChart.setOption(phoneHighOption);

var phoneMidOption = {
    title: {
        text: "中端产品按厂商统计",
        textAlign: "center",
        top: 20,
        left: "50%",
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "white",
            fontWeight: 0,
            fontSize: 30
        }
    },
    legend: {
        data:['严重漏洞','高危漏洞','中危漏洞','低危漏洞','产品安全指数'],
        bottom: 20,
        selectedMode: false,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#AAAEBB",
            fontSize: 16
        }
    },
    grid: {
        top: '20%',
        left: '5%',
        right: '5%',
        bottom: '10%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['OEM1', 'OEM2', 'OEM3', 'OEM4', 'OEM5', 'OEM6', 'OEM7', 'OEM8', 'OEM9', 'OEM10', '其他'],
            data: phoneMidOrderNameList,
            axisLabel: {
                margin: 15,
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            name: "未修复漏洞数",
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#AAAEBB",
                fontSize: 16
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            },
            splitLine: {
                show: false
            }
        },
        {
        	type : 'value',
            name: "产品安全指数",
            min: 0,
            max: 100,
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#AAAEBB",
                fontSize: 16
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#639DFE"],
    series : [
        {
            name:'低危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneMidLowRiskNumList,
            barWidth: 25
        },
        {
            name:'中危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneMidMidRiskNumList,
            barWidth: 25
        },
        {
            name:'高危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneMidHighRiskNumList,
            barWidth: 25
        },
        {
            name:'严重漏洞',
            type:'bar',
            stack: '漏洞',
            // data: [8, 10, 7, 12, 10, 10, 8, 10, 7, 12, 10],
            data: phoneMidSeriousRiskNumList,
            barWidth: 25
        },
        {
            name:'产品安全指数',
            type:'line',
            yAxisIndex: 1,
			// data: [70, 60, 50, 45, 40, 40, 32, 44, 50]
            data: phoneMidSafetyScoreList,
            symbol: "circle",
            symbolSize: 8,
            hoverAnimation: false,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
            // markPoint: {data: phoneMidMarkPoints}
        }
    ]
};
var phoneMidChart = echarts.init($("#phone-mid-chart")[0]);
phoneMidChart.setOption(phoneMidOption);

var phoneLowOption = {
    title: {
        text: "入门产品按厂商统计",
        textAlign: "center",
        top: 20,
        left: "50%",
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "white",
            fontWeight: 0,
            fontSize: 30
        }
    },
    legend: {
        data:['严重漏洞','高危漏洞','中危漏洞','低危漏洞','产品安全指数'],
        bottom: 20,
        selectedMode: false,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#AAAEBB",
            fontSize: 16
        }
    },
    grid: {
        top: '20%',
        left: '5%',
        right: '5%',
        bottom: '10%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['OEM1', 'OEM2', 'OEM3', 'OEM4', 'OEM5', 'OEM6', 'OEM7', 'OEM8', 'OEM9', 'OEM10', '其他'],
            data: phoneLowOrderNameList,
            axisLabel: {
                margin: 15,
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            name: "未修复漏洞数",
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#AAAEBB",
                fontSize: 16
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            },
            splitLine: {
                show: false
            }
        },
        {
        	type : 'value',
            name: "产品安全指数",
            min: 0,
            max: 100,
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#AAAEBB",
                fontSize: 16
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#AAAEBB",
                    fontSize: 16
            	}
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#639DFE"],
    series : [
        {
            name:'低危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneLowLowRiskNumList,
            barWidth: 25
        },
        {
            name:'中危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneLowMidRiskNumList,
            barWidth: 25
        },
        {
            name:'高危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: phoneLowHighRiskNumList,
            barWidth: 25
        },
        {
            name:'严重漏洞',
            type:'bar',
            stack: '漏洞',
            // data: [8, 10, 7, 12, 10, 10, 8, 10, 7, 12, 10],
            data: phoneLowSeriousRiskNumList,
            barWidth: 25
        },
        {
            name:'产品安全指数',
            type:'line',
            yAxisIndex: 1,
			// data: [70, 60, 50, 45, 40, 40, 32, 44, 50, 47]
            data: phoneLowSafetyScoreList,
            symbol: "circle",
            symbolSize: 8,
            hoverAnimation: false,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
            // markPoint: {data: phoneLowMarkPoints}
        }
    ]
};
var phoneLowChart = echarts.init($("#phone-low-chart")[0]);
phoneLowChart.setOption(phoneLowOption);