var pageTime = 10000;
var tabTime = 5000;
var largeTabTime = 3000;
var dimension = 1;
var pause = false;
var start = true;
var nowTimeout;
var nextTimeout;
var timeouts = [];

var funcs = [
    refreshPageTab,
    refreshPageTab,
    refreshTab,
    refreshTab,
    refreshPageTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshLargeTab,
    refreshPageTab,
    repush
];

var runFuncs = [];
var notRunFuncs = new Array(funcs);

$(function(){
    $(".title").click(pauseToggle);
    $(".footer").click(pauseToggle);
    $(".dimension-group").click(pauseToggle);

    repush();

    $(".tab-label").click(function () {
        if ($(this).hasClass("active")) {
            return false;
        }

        clearAllTimeout();
        var nowTabIndex = $(".tab-label.active").index();
        var thisIndex = $(this).index();
        var distance = thisIndex - nowTabIndex;
        var pauseBak = pause;
        pause = false;
        if (distance < 0) { // prev
            for (var i = 0;i < -distance;i++) {
                prevTab();
            }
            var funcs = runFuncs.splice(distance, -distance);
            for (var i = 0;i < notRunFuncs.length;i++) {
                funcs.push(notRunFuncs[i])
            }
            notRunFuncs = funcs;
            $(document).queue("switch", notRunFuncs);
            next(tabTime);
        } else { // after
            for (var i = 0;i < distance;i++) {
                next(0);
            }
        }
        pause = pauseBak;
        return false;
    });

    $(".tab-large-label").click(function () {
        if ($(this).hasClass("active")) {
            return false;
        }

        clearAllTimeout();
        var nowLargeTabIndex = $(".tab-large-label.active").index();
        var thisIndex = $(this).index();
        var distance = thisIndex - nowLargeTabIndex;
        var pauseBak = pause;
        pause = false;
        if (distance < 0) { // prev
            toLargeTab($(this));
            var funcs = runFuncs.splice(distance, -distance);
            for (var i = 0;i < notRunFuncs.length;i++) {
                funcs.push(notRunFuncs[i])
            }
            notRunFuncs = funcs;
            $(document).queue("switch", notRunFuncs);
            next(largeTabTime);
        } else { // after
            toLargeTab($(this));
            var funcs = [];
            for (var i = 0;i < runFuncs.length;i++) {
                funcs.push(runFuncs[i])
            }
            for (var i = 0;i < distance;i++) {
                funcs.push(notRunFuncs[i]);
            }
            runFuncs = funcs;
            notRunFuncs.splice(0, distance);
            $(document).queue("switch", notRunFuncs);
            next(largeTabTime);
        }
        pause = pauseBak;
        return false;
    });

    // page controller
    $(".page-controller").mouseenter(function () {
        $(this).animate({opacity: 1}, 200);
    }).mouseleave(function () {
        $(this).animate({opacity: 0}, 200);
    });

    $(".page-controller-prev").click(function () {
        var pageFuncIndex = -10;
        var prevFuncIndex = -10;
        $(runFuncs).each(function (index, item) {
            if (item == refreshPageTab) {
                prevFuncIndex = pageFuncIndex;
                pageFuncIndex = index;
            }
        });
        if (prevFuncIndex == -10) {
            if (pageFuncIndex == -10) {
                $(notRunFuncs).each(function (index, item) {
                    if (item == refreshPageTab) {
                        prevFuncIndex = pageFuncIndex;
                        pageFuncIndex = index;
                    }
                });
                var spliceFuncs = notRunFuncs.splice(prevFuncIndex + 1, notRunFuncs.length - prevFuncIndex - 1);
                runFuncs = notRunFuncs;
                notRunFuncs = spliceFuncs;
                $(document).queue("switch", spliceFuncs);
            } else {
                $(notRunFuncs).each(function (index, item) {
                    if (item == refreshPageTab) {
                        pageFuncIndex = index;
                    }
                });
                var spliceFuncs = notRunFuncs.splice(pageFuncIndex + 1, notRunFuncs.length - pageFuncIndex - 1);
                for (var i = 0;i < notRunFuncs.length;i++) {
                    runFuncs.push(notRunFuncs[i]);
                }
                notRunFuncs = spliceFuncs;
                $(document).queue("switch", spliceFuncs);
            }
        } else {
            var funcs = runFuncs.splice(prevFuncIndex + 1, runFuncs.length - prevFuncIndex - 1);
            for (var i = 0;i < notRunFuncs.length;i++) {
                funcs.push(notRunFuncs[i]);
            }
            notRunFuncs = funcs;
            $(document).queue("switch", funcs);
        }
        prevPage();
    });
    $(".page-controller-next").click(function () {
        var pageFuncIndex = -10;
        $(notRunFuncs).each(function (index, item) {
            if (item == refreshPageTab && pageFuncIndex == -10) {
                pageFuncIndex = index;
            }
        });
        if (pageFuncIndex == -10) {
            repush();
            return;
        }
        var spliceFuncs = notRunFuncs.splice(0, pageFuncIndex);
        for (var i = 0;i < spliceFuncs.length;i++) {
            runFuncs.push(spliceFuncs[i]);
        }
        $(document).queue("switch", notRunFuncs);
        var pauseBak = pause;
        pause = false;
        next(0);
        pause = pauseBak;
    });
});

function next(timeout) {
    nowTimeout = timeout;
    clearAllTimeout();
    if (timeout <= 0) {
        if (!pause) {
            runFuncs.push(notRunFuncs[0]);
            $(document).dequeue("switch");
            notRunFuncs.splice(0, 1);
        }
    } else {
        nextTimeout = setTimeout(function () {
            if (!pause) {
                runFuncs.push(notRunFuncs[0]);
                $(document).dequeue("switch");
                notRunFuncs.splice(0, 1);
            }
        }, timeout);
        timeouts.push(nextTimeout);
    }
}

function repush() {
    runFuncs = [];
    notRunFuncs = [];
    for (var i = 0;i < funcs.length;i++) {
        notRunFuncs.push(funcs[i]);
    }
    $(document).queue("switch", funcs);
    if (start) {
        next(pageTime);
        start = false;
    } else {
        resetTabs();
        next(0);
    }
}

// pause and start
function pauseToggle() {
    pause = !pause;
    if (!pause) {
        $("#start-img").animate({
            left: 128,
            bottom: -10,
            width: 100,
            height: 100,
            opacity: 0
        },250);
        clearAllTimeout();
        next(0);
    } else {
        $("#start-img").animate({
            left: 148,
            bottom: 0,
            width: 60,
            height: 60,
            opacity: 1
        },250);
    }
}

function prevTab() {

    var activeTabLabel = $(".tab-label.active");
    var prevTabLabel = activeTabLabel.prev();
    prevTabLabel.addClass("active");
    activeTabLabel.removeClass("active");

    $(".tab-panel").animate({left: 0}, 1000, function() {
        $(this).children("div:last").insertBefore($(this).children("div:first"));
        $(this).css("left", "-100%");
    });
}

function refreshTab() {

    var activeTabLabel = $(".tab-label.active");
    var nextTabLabel = activeTabLabel.next();
    nextTabLabel.addClass("active");
    activeTabLabel.removeClass("active");

    $(".tab-panel").animate({left: '-200%'}, 1000, function() {
        $(this).children("div:first").insertAfter($(this).children("div:last"));
        $(this).css("left", "-100%");
    });

    setTimeout("next(" + tabTime + ")", 1000);
};

function toLargeTab($toTab) {
    var activeTabLabel = $(".tab-large-label.active");
    var index = activeTabLabel.index();
    var nextIndex;
    $toTab.addClass("active");
    nextIndex = $toTab.index();
    if (index == nextIndex) return;
    activeTabLabel.removeClass("active");
    if ($(".tab-large-label.active").length <= 0) {
        $toTab.addClass("active");
    }

    $(".tab-large-panel-container").each(function () {
        var $container = $(this);
        var callback  = function($container) {
            return function() {
                $($container.children(".tab-large-panel").get(index)).css("display", "none");
                $($container.children(".tab-large-panel").get(nextIndex)).css("opacity", 0);
                $($container.children(".tab-large-panel").get(nextIndex)).css("display", "block");
                $($container.children(".tab-large-panel").get(nextIndex)).animate({opacity: 1}, 250);
            }
        }($container);
        $($container.children(".tab-large-panel").get(index)).animate({opacity: 0}, 250, callback);
    });
}

function refreshLargeTab() {
    var activeTabLabel = $(".tab-large-label.active");
    var nextTabLabel = activeTabLabel.next();
    var firstTabLabel = activeTabLabel.parent().children(":first");
    var index = activeTabLabel.index();
    var nextIndex;
    if (nextTabLabel.html() != undefined) {
        nextTabLabel.addClass("active");
        nextIndex = nextTabLabel.index();
    } else {
        firstTabLabel.addClass("active");
        nextIndex = firstTabLabel.index();
    }
    activeTabLabel.removeClass("active");

    $(".tab-large-panel-container").each(function () {
        var $container = $(this);
        var callback  = function($container) {
            return function() {
                $($container.children(".tab-large-panel").get(index)).css("display", "none");
                $($container.children(".tab-large-panel").get(nextIndex)).css("opacity", 0);
                $($container.children(".tab-large-panel").get(nextIndex)).css("display", "block");
                $($container.children(".tab-large-panel").get(nextIndex)).animate({opacity: 1}, 250);
            }
        }($container);
        $($container.children(".tab-large-panel").get(index)).animate({opacity: 0}, 250, callback);
    });

    setTimeout("next(" + largeTabTime + ")", 250);
}

function prevPage() {
    var activeTabLabel = $(".dimension.active");
    var prevTabLabel = activeTabLabel.prev();
    prevTabLabel.addClass("active");
    activeTabLabel.removeClass("active");

    if (dimension == 1) {
        dimension = 4;
    } else if (dimension == 2) {
        dimension = 1;
    } else if (dimension == 3) {
        dimension = 2;
    } else if (dimension == 4) {
        dimension = 3;
    }

    clearAllCharts(dimension);
    $(".dimension-group").animate({left: 0}, 1000, function(){
        $(this).children(".dimension:last").insertBefore($(this).children(".dimension:first"));
        $(this).css("left", "-100%");

        repaintAllCharts();
        resetTabs();
        if(dimension == 1) {
            next(pageTime);
        } else if(dimension == 2) {
            next(pageTime);
        } else if(dimension == 3) {
            next(tabTime);
        } else if(dimension == 4) {
            next(largeTabTime);
        }
    });
}

function refreshPageTab() {
    var activeTabLabel = $(".dimension.active");
    var nextTabLabel = activeTabLabel.next();
    nextTabLabel.addClass("active");
    activeTabLabel.removeClass("active");

    if (dimension == 1) {
        dimension = 2;
    } else if (dimension == 2) {
        dimension = 3;
    } else if (dimension == 3) {
        dimension = 4;
    } else if (dimension == 4) {
        dimension = 1;
    }

    clearAllCharts(dimension);
    $(".dimension-group").animate({left:'-200%'}, 1000, function(){
        $(this).children(".dimension:first").insertAfter($(this).children(".dimension:last"));
        $(this).css("left", "-100%");

        repaintAllCharts();
        resetTabs();
        if(dimension == 1) {
            next(pageTime);
        } else if(dimension == 2) {
            next(pageTime);
        } else if(dimension == 3) {
            next(tabTime);
        } else if(dimension == 4) {
            next(largeTabTime);
        }
    });
}

function resetTabs() {

    // dimension 2
    var activeTabLabel = $(".tab-label.active");
    var firstTabLabel = $(".tab-label:first");

    firstTabLabel.addClass("active");
    activeTabLabel.removeClass("active");
    if ($(".tab-label.active").length <= 0) {
        firstTabLabel.addClass("active");
    }

    // $(".tab-panel").animate({left: '-200%'}, 1000, function() {
    //     $(this).children("div:first").insertAfter($(this).children("div:last"));
    //     $(this).css("left", "-100%");
    // });
    $(".tab-panel").each(function () {
        var $first = $(this).find(".first");
        if ($first.prev() == undefined) return;
        else {
            var firstIndex = $first.index();
            for (var i = 0;i < firstIndex;i++) {
                $first.parent().children(":first").insertAfter($first.parent().children(":last"));
            }
        }
    });

    // dimension 3
    // var activeTabLabel = $(".tab-large-label.active");
    var firstLargeTabLabel = $(".tab-large-label:first");
    toLargeTab(firstLargeTabLabel);
    // var index = activeTabLabel.index();
    // var nextIndex;
    // firstTabLabel.addClass("active");
    // nextIndex = firstTabLabel.index();
    // activeTabLabel.removeClass("active");

    // $(".tab-large-panel-container").each(function () {
    //     var $container = $(this);
    //     var callback  = function($container) {
    //         return function() {
    //             $($container.children(".tab-large-panel").get(index)).css("display", "none");
    //             $($container.children(".tab-large-panel").get(nextIndex)).css("opacity", 0);
    //             $($container.children(".tab-large-panel").get(nextIndex)).css("display", "block");
    //             $($container.children(".tab-large-panel").get(nextIndex)).animate({opacity: 1}, 250);
    //         }
    //     }($container);
    //     $($container.children(".tab-large-panel").get(index)).animate({opacity: 0}, 250, callback);
    // });
}

function clearAllCharts(dimension) {
    if (dimension == 1) {
        if ("undefined" != typeof safetyTrendChart) {
            safetyTrendChart = echarts.init($("#safety-trend-chart")[0]);
        }
        unfixedTrendChart = echarts.init($("#unfixed-trend-chart")[0]);
    } else if (dimension == 2) {
        oemChart = echarts.init($("#oem-chart")[0]);
        odmChart = echarts.init($("#odm-chart")[0]);
        chipChart = echarts.init($("#chip-chart")[0]);
        if ("undefined" != typeof androidChart) {
            androidChart = echarts.init($("#android-chart")[0]);
        }
    } else if (dimension == 3){
        phoneHighChart = echarts.init($("#phone-high-chart")[0]);
        phoneMidChart = echarts.init($("#phone-mid-chart")[0]);
        phoneLowChart = echarts.init($("#phone-low-chart")[0]);
    }
    $(".chart-title").hide();
}

function repaintAllCharts() {
    if ("undefined" != typeof safetyTrendChart) {
        safetyTrendChart.setOption(safetyTrendOption);
    }
    unfixedTrendChart.setOption(unfixedTrendOption);
    oemChart.setOption(oemOption);
    odmChart.setOption(odmOption);
    chipChart.setOption(chipOption);
    if ("undefined" != typeof androidChart) {
        androidChart.setOption(androidOption);
    }
    phoneHighChart.setOption(phoneHighOption);
    phoneMidChart.setOption(phoneMidOption);
    phoneLowChart.setOption(phoneLowOption);
    $(".chart-title").show();
}

function clearAllTimeout() {
    for (var i = 0;i < timeouts.length;i++) {
        clearTimeout(timeouts[i]);
    }
}