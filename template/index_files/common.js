/**
 * Created by liqifei on 6/15/17.
 */

// if data < 3 then hide bar label
function barLabelFilter(dataList) {
    $(dataList).each(function (index, item) {
        if (item < 3) {
            dataList[index] = {value: item, label: {normal: {show: false}}};
        }
    });
}

function getAllMarkPoint(dataList) {
    var markPoints = [];
    $(dataList).each(function (index, item) {
        var markPoint = {
            symbol: "circle",
            symbolSize: 25,
            value: item,
            xAxis: index,
            yAxis: item,
            label: {normal: {textStyle: {fontFamily: "Microsoft YaHei", color: "black", fontSize: 16, fontWeight: 1000}}}
        };
        markPoints.push(markPoint)
    });
    return markPoints;
}

function getAllRiskNum(list, lowList, midList, highList, seriousList) {
    var result = [];

    $(list).each(function (index, item) {
        var count = 0;
        count += lowList[index] ? lowList[index] : 0;
        count += midList[index] ? midList[index] : 0;
        count += highList[index] ? highList[index] : 0;
        count += seriousList[index] ? seriousList[index] : 0;
        result.push(count);
    });

    return result;
}