// chart
var oemOption = {
    legend: {
        data:['严重漏洞','高危漏洞','中危漏洞','低危漏洞','产品安全指数'],
        bottom: 40,
        selectedMode: false,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#A2A6B4",
            fontSize: 12
        }
    },
    grid: {
        top: '10%',
        left: '5%',
        right: '5%',
        bottom: '20%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['OEM1', 'OEM2', 'OEM3', 'OEM4', 'OEM5', 'OEM6', 'OEM7', 'OEM8', 'OEM9', 'OEM10', '其他'],
            data: oemOrderNameList,
            axisLabel: {
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
                }
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            name: "未修复漏洞数",
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#A2A6B4",
                fontSize: 12
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
                }
            },
            splitLine: {
                show: false
            }
        },
        {
        	type : 'value',
            name: "产品安全指数",
            min: 0,
            max: 100,
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#A2A6B4",
                fontSize: 12
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
            	}
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#639DFE"],
    series : [
        {
            name:'低危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: oemLowRiskNumList,
            barWidth: 25
        },
        {
            name:'中危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: oemMidRiskNumList,
            barWidth: 25
        },
        {
            name:'高危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32, 25, 25, 20, 25, 27, 32, 25],
            data: oemHighRiskNumList,
            barWidth: 25
        },
        {
            name:'严重漏洞',
            type:'bar',
            stack: '漏洞',
            // data: [8, 10, 7, 12, 10, 10, 8, 10, 7, 12, 10],
            data: oemSeriousRiskNumList,
            barWidth: 25
        },
        {
            name:'产品安全指数',
            type:'line',
            yAxisIndex: 1,
			// data: [70, 60, 50, 45, 40, 40, 32, 44, 50, 47, 60]
            data: oemSafetyScoreList,
            symbol: "circle",
            symbolSize: 8,
            hoverAnimation: false,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
            // markPoint: {data: oemMarkPoints}
        }
    ]
};
var oemChart = echarts.init($("#oem-chart")[0]);
oemChart.setOption(oemOption);


var odmOption = {
    legend: {
        data:['严重漏洞','高危漏洞','中危漏洞','低危漏洞','产品安全指数'],
        bottom: 40,
        selectedMode: false,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#A2A6B4",
            fontSize: 12
        }
    },
    grid: {
        top: '10%',
        left: '5%',
        right: '5%',
        bottom: '20%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['ODM1', 'ODM2', 'ODM3', 'ODM4'],
            data: odmOrderNameList,
            axisLabel: {
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
                }
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            name: "未修复漏洞数",
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#A2A6B4",
                fontSize: 12
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
                }
            },
            splitLine: {
                show: false
            }
        },
        {
        	type : 'value',
            name: "产品安全指数",
            min: 0,
            max: 100,
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#A2A6B4",
                fontSize: 12
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
            	}
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#639DFE"],
    series : [
        {
            name:'低危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32],
            data: odmLowRiskNumList,
            barWidth: 25
        },
        {
            name:'中危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32],
            data: odmMidRiskNumList,
            barWidth: 25
        },
        {
            name:'高危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27, 32],
            data: odmHighRiskNumList,
            barWidth: 25
        },
        {
            name:'严重漏洞',
            type:'bar',
            stack: '漏洞',
            // data: [8, 10, 7, 12],
            data: odmSeriousRiskNumList,
            barWidth: 25
        },
        {
            name:'产品安全指数',
            type:'line',
            yAxisIndex: 1,
			// data: [70, 50, 45, 40]
            data: odmSafetyScoreList,
            symbol: "circle",
            symbolSize: 8,
            hoverAnimation: false,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
            // markPoint: {data: odmMarkPoints}
        }
    ]
};
var odmChart = echarts.init($("#odm-chart")[0]);
odmChart.setOption(odmOption);


var chipOption = {
    legend: {
        data:['严重漏洞','高危漏洞','中危漏洞','低危漏洞','产品安全指数'],
        bottom: 40,
        selectedMode: false,
        textStyle: {
            fontFamily: "Microsoft YaHei",
            color: "#A2A6B4",
            fontSize: 12
        }
    },
    grid: {
        top: '10%',
        left: '7%',
        right: '7%',
        bottom: '20%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // data : ['Qualcomm', 'MTK', '其他'],
            data: chipOrderNameList,
            axisLabel: {
                textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
                }
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            name: "未修复漏洞数",
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#A2A6B4",
                fontSize: 12
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
            	}
            },
            splitLine: {
                show: false
            }
        },
        {
        	type : 'value',
            name: "产品安全指数",
            min: 0,
            max: 100,
            nameTextStyle: {
                fontFamily: "Microsoft YaHei",
                color: "#A2A6B4",
                fontSize: 12
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
            	textStyle: {
                    fontFamily: "Microsoft YaHei",
                    color: "#A2A6B4",
                    fontSize: 12
            	}
            },
            splitLine: {
                lineStyle: {
                    color: "rgba(255, 255, 255, 0.1)"
                }
            }
        }
    ],
    color: ["#FCD152", "#FF9800", "#FE7642", "#EF3C30", "#639DFE"],
    series : [
        {
            name:'低危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27],
            data: chipLowRiskNumList,
            barWidth: 25
        },
        {
            name:'中危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27],
            data: chipMidRiskNumList,
            barWidth: 25
        },
        {
            name:'高危漏洞',
            type:'bar',
            stack: '漏洞',
            // data:[20, 25, 27],
            data: chipHighRiskNumList,
            barWidth: 25
        },
        {
            name:'严重漏洞',
            type:'bar',
            stack: '漏洞',
            // data: [8, 10, 7],
            data: chipSeriousRiskNumList,
            barWidth: 25
        },
        {
            name:'产品安全指数',
            type:'line',
            yAxisIndex: 1,
			// data: [70, 50, 45]
            data: chipSafetyScoreList,
            symbol: "circle",
            symbolSize: 8,
            hoverAnimation: false,
            label: {
                normal: {
                    show: true,
                    position: "top",
                    textStyle: {
                        fontFamily: "Microsoft YaHei",
                        color: "white",
                        fontWeight: "bolder",
                        fontSize: 16
                    }
                }
            },
            // markPoint: {data: chipMarkPoints}
        }
    ]
};
var chipChart = echarts.init($("#chip-chart")[0]);
chipChart.setOption(chipOption);