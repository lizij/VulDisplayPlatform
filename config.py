class Config:
    SECRET_KEY = "secret key"
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True
    TEMPLATES_AUTO_RELOAD = True
    SQLALCHEMY_DATABASE_URI = "mysql://root:root@127.0.0.1/vuldisplay?charset=utf8mb4"

config = {
    "default":DevelopmentConfig
}

