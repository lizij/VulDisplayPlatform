[TOC]

# 主页设计(app/main/index) /

## 命名关键词

### 漏洞

* vinfo：漏洞信息
- vnum：漏洞数量
- vseverity：漏洞等级
- vtype：漏洞类型


### 设备

* dinfo：设备信息
* dnum：设备数量

### 其他

- month：月份
- android：安卓版本
- kernel：内核版本
- watching_vids：关注漏洞
- vendor：厂商

## 直方图/折线图部分

命名逻辑：x轴名\_y轴名\_图例名_chart

直方图使用数据堆叠，最上方显示总数，即sum(y1, y2, y3, ...)

折线图不使用数据堆叠，分别显示数量yi

### 按月份统计

#### 月份+漏洞等级 /month_vnum_vseverity_chart

直方图，月份（x），漏洞数量（y），漏洞等级

![](直方图/月份数量等级.png)

#### 月份+Android版本 /month_vnum_android_chart

直方图，月份（x），漏洞数量（y），Android版本

![](直方图/月份数量安卓版本.png)

#### 月份+关注漏洞 /month_vnum_watching_vids_chart

折线图，月份（x），漏洞数量（y），关注漏洞legend

![](直方图\月份数量典型漏洞.png)

### 按Android版本统计 /android_vnum_vseverity_chart

直方图，Android版本（x），漏洞数量（y），漏洞等级

![](直方图/Android版本数量等级.png)

### 按内核版本统计 /kernel_vnum_vseverity_chart

直方图，内核版本（x），漏洞数量（y），漏洞等级

![](直方图/内核数量等级.png)

### 按厂商统计 /vendor_vnum_vseverity_chart

直方图，厂商（x），数量（y），漏洞等级

![](直方图/厂商数量等级.png)

## 饼图部分

命名逻辑：数据x\_图例\_pie

### 按漏洞等级分布 /vnum_vseverity_pie

![](饼图\数量等级分布.png)

### 按设备漏洞数量分布 /dnum_vnum_pie

![](饼图\终端数量分布.png)

### 按漏洞类型分布 /vnum_vtype_pie

![](饼图\漏洞类型分布.png)

## 排行部分

命名逻辑：排名内容\_关键字\_rank

### 设备漏洞排行 /dinfo_vnum_rank

设备信息，漏洞数量

![](排行\设备_漏洞数量.png)

### 漏洞检出排行 /vinfo_vnum_rank

漏洞信息，漏洞数量

![](其他/漏洞次数排行.png)

## 其他部分

命名逻辑：根据内容

### totals

检出漏洞总数，检出严重漏洞总数

![](其他/当前漏洞数.png)

终端数，漏洞数

![](其他/终端数漏洞数.png)

### 关键漏洞数 /vinfo_keywords_count

根据常见漏洞关键字检索漏洞名

![](其他/关键漏洞.png)

# 报告页设计(app/main/report.py) /report

使用表格展示所有历史检测任务

* 点击`查看详情`：展示详细检测的漏洞内容
* 点击`报告下载`：将报告内容转换为csv并下载

![](报告\报告首页.png)

# 用户设计(app/user)

## 注册设计 /signin

注册后需要管理员审核，审核通过后才可以正常登陆

![册界](用户\注册界面.png)

## 登陆设计 /login

![](用户\登陆界面.png)

## 用户管理设计 /manage

### 管理员

* 可以查看全部漏洞数据，管理漏洞信息
* 可以管理用户信息，激活/冻结用户，设定用户名，关注漏洞，将来可以设计具体可以查看哪些图表
* 可以查看全部报告数据


![](用户/管理员用户界面.png)

### 厂商

* 可以查看最新漏洞数据
* 可以查看该厂商相关的内容
  * 不能查看其它厂商的漏洞数量统计
  * 只能查看自己的产品危险排行
  * 只能查看自己的报告数据

![](用户/厂商用户界面.png)

### 来宾用户

* 可以查看最新漏洞数据

![](用户\来宾用户界面.png)

# 数据库设计

```mysql
create table device_info
(
	device_id varchar(50) not null comment '设备型号'
		primary key,
	abi varchar(50) null comment '应用二进制接口',
	vendor varchar(100) default '未知' null comment '供应商',
	android_version varchar(20) null comment 'Android版本号',
	kernel varchar(20) null comment '内核版本号'
)
comment '设备信息' engine=InnoDB charset=utf8
;

create table task_info
(
	device_id varchar(50) not null comment '设备型号',
	vid varchar(50) default '' not null comment '漏洞编号',
	tid varchar(20) not null comment '任务ID',
	time timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '检测时间',
	primary key (device_id, vid)
)
comment '检测任务信息' engine=InnoDB charset=utf8
;

create table user_info
(
	username varchar(50) not null comment '用户名'
		primary key,
	password varchar(50) null comment '密码',
	vendor varchar(100) null comment '厂商名',
	type varchar(10) default 'guest' null comment '账户类型，默认为来宾',
	state varchar(10) default 'inactive' null comment '用户状态，默认为inactive，可以激活为active',
	options json null comment '用户选项',
	email varchar(30) default '' null comment '邮箱',
	phone varchar(20) default '' null comment '电话'
)
comment '用户表' engine=InnoDB charset=utf8
;

create table vul_info
(
	vid varchar(50) not null comment 'CVE/CNNVD/CNVD编号'
		primary key,
	description varchar(500) null comment '描述',
	severity varchar(10) null comment '等级，包括低危，中危，高危或严重',
	type varchar(50) null comment '类型',
	pub_date timestamp null comment '公布日期',
	name varchar(50) null comment '名称'
)
comment '漏洞信息' engine=InnoDB charset=utf8
;
```

# 代码结构设计

## 文件结构说明

```shell
│   .gitignore
│   app.py # 程序运行入口
│   config.py # 程序配置文件，一般不需要修改
│   gen_sql_data.py # 数据生成脚本，需要配合http://git.ifseclabs.com/vuln/vda工程使用
│   Readmd.md
│   requirements.txt # 依赖库
|
├───app
│   │   models.py # 数据库模型
│   │   __init__.py # app包初始化文件，主要为初始化并配置Flask app
│   │
│   ├───main # 主要展示部分，包括主页和报告页
│   │   │   errors.py # 错误页路由
│   │   │   report.py # 报告页路由
│   │   │   __init__.py # main包初始化，注册index，report和errors
│   │   │
│   │   ├───index # 主页部分 /
│   │           charts.py # 直方图数据请求
│   │           others.py # 其他数据请求
│   │           pie.py # 饼图数据请求
│   │           rank.py # 排行数据请求
│   │           __init__.py # index包初始化
│   │
│   ├───static
│   │   ├───css
│   │   │       bootstrap.min.css # bootstrap css，请勿修改
│   │   │       bootstrap.min.css.map # bootstrap css map，请勿修改
│   │   │       styles.css # 自定义css
│   │   │
│   │   ├───images # 图片资源
│   │   │
│   │   └───js
│   │           bootstrap.min.js # bootstrap库
│   │           bootstrap.min.js.map # bootstrap js map，请勿修改
│   │           ChartRender.js # 用于生成echarts option，一般不需要修改
│   │           common.js # 其他js函数，目前尚未使用
│   │           echarts.js # echarts库
│   │           jquery.min.js # jquery库
│   │
│   ├───templates
│   │       404.html # 404页模板
│   │       500.html # 500页模板
│   │       base.html # 基础模板，用于生成其他模板
│   │       index.html # 主页模板
│   │       login.html # 登录页模板
│   │       manage.html # 用户管理页模板
│   │       profile.html # 个人信息页模板
│   │       report.html # 报告页模板
│   │       signin.html # 注册页模板
│   │
│   ├───user # 用户部分，包括登录、注册和用户管理
│           forms.py # 表单定义
│           views.py # 路由定义
│           __init__.py # user包初始化
├───docs
│   │   design.md # 设计文档
│   │
│   ├───其他
│   │       产品风险排行.png
│   │       关键漏洞.png
│   │       当前漏洞数.png
│   │       漏洞次数排行.png
│   │       终端数漏洞数.png
│   │
│   ├───报告
│   │       报告首页.png
│   │
│   ├───排行
│   │       设备_漏洞数量.png
│   │
│   ├───用户
│   │       厂商用户界面.png
│   │       来宾用户界面.png
│   │       注册界面.png
│   │       登陆界面.png
│   │       管理员用户界面.png
│   │
│   ├───直方图
│   │       Android版本数量等级.png
│   │       内核数量等级.png
│   │       厂商数量等级.png
│   │       月份数量典型漏洞.png
│   │       月份数量安卓版本.png
│   │       月份数量等级.png
│   │
│   └───饼图
│           数量等级分布.png
│           漏洞类型分布.png
│           终端数量分布.png
│
├───template # 安天平台示例
```

## echarts使用说明

### echarts背景简介

echarts是百度推出的开源图表显示框架，本身使用比较复杂，有很多的配置参数，这里给出一个简单的示例

echarts使用时至少需要3样内容：

* 一个定义好大小的div
  * 建议除了大小什么都不要定义，可能会没有作用，也可能会影响echarts渲染效果
  * 必须要**显式定义大小**，使用style参数配置width和height，可以是px或百分比，但不能使用class来指定（原因未知）
* 定义图表显示样式的option
  * 字典格式，主要是为了配置图表的类型（直方图、折线图或饼图等）、标题、图例、数据等等
  * 重新设置option会导致图表重绘
* 符合格式的数据
  * 对于直方图或折线图，需要x轴数据（一维数组），y轴数据（二维数组）和图例（一维数组）
  * 对于饼图，需要数据（一维数组）和图例（一维数组）

以下是摘自echarts官方文档的一段示例代码

```html
<!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
<div id="main" style="width: 600px;height:400px;"></div>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));

    // 指定图表的配置项和数据
    var option = {
        title: {
            text: 'ECharts 入门示例'
        },
        legend: {
            data:['销量']
        },
        xAxis: {
            data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
        },
        yAxis: {},
        series: [{
            name: '销量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
        }]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
</script>
```

其他部分可以参考[教程](http://echarts.baidu.com/tutorial.html)和[示例](http://echarts.baidu.com/examples/)

### 本项目中的图表设计

直方图/折线图和饼图的设计是类似的，以下配合代码给出示例`month_vnum_vseverity_chart`

#### 后端服务

首先，需要**定义返回图表数据的函数和路由**，所有的图表数据路由都定义在`app/main/index`下的py文件中，根据图表的类型选择合适的py文件并在其中定义函数，记得**严格遵守前文提到的命名规范**

```python
LAST_12_MONTH = """date_format(task_info.time, '%%Y-%%m') > date_format(date_sub(curdate(), INTERVAL 12 MONTH), '%%Y-%%m')"""
MONTH = """date_format(task_info.time, '%%Y-%%m')"""

@main.route("/month_vnum_vseverity_chart", methods=["GET"])
def month_vnum_vseverity_chart():
    """
    时间x 漏洞数量y 漏洞等级legend
    :return:
    """
    try:
        sql = """
            SELECT {} AS month, vul_info.severity, count(*) AS num
            FROM task_info, vul_info
            WHERE task_info.vid = vul_info.vid AND {}
            GROUP BY month, severity
            ORDER BY month;
            """.format(MONTH, LAST_12_MONTH)
        rows = db.engine.execute(sql)
        return generate_chart_data(rows, severity_legends)
    except:
        traceback.print_exc()
        return ""
    
def generate_chart_data(rows, legends, anonymous=False):
    """
    自动生成直方图/折线数据
    :param rows: 每一行必须只包含3个数据，分别对应x，legend和y
    :param legends: 使用的legends名称
    :param anonymous: 是否要进行匿名
    :return: dict，对应x，y，label 3个数组，x为x轴数据，y中每一个数组都对应一个legend和x轴的数据，label为该列显示的名字
    """

    res = {"x": [],
           "y": [[] for i in range(0, len(legends))],
           # "label": [],
           "legend": legends}
    cur = -1
    for row in rows:
        x_data = row[0]
        legend = row[1]
        count = int(row[2])
        if not x_data in res["x"]:
            res["x"].append(x_data)
            cur += 1
            for y in res["y"]:
                y.append(0)
            # res["label"].append(0)
        res["y"][legends.index(legend)][cur] += count
        # res["label"][cur] += count
    if anonymous:
        do_anonymous(res['x'])

    return json.dumps(res, ensure_ascii=False)    
```

产生数据的方式就是通过sql查询数据库，得到需要的x轴，y轴和对应图例的数据，格式上类似于

| month   | severity | num  |
| ------- | -------- | ---- |
| 2017-06 | 严重     | 35   |
| 2017-06 | 中危     | 2    |
| 2017-06 | 高危     | 3    |
| 2017-07 | 严重     | 22   |
| 2017-07 | 中危     | 2    |

最终访问该url返回的数据格式为

```json
{
    "legend": ["低危", "中危", "高危", "严重"],
    "x": ["2017-06", "2017-07", "2017-08", "2017-09", "2017-10", "2017-11", "2017-12", "2018-01", "2018-02", "2018-03", "2018-04"], 
    "y": [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
        [2, 2, 426, 542, 242, 308, 189, 325, 105, 82, 74], 
        [3, 2, 685, 799, 372, 482, 316, 504, 170, 150, 118], 
        [35, 22, 1077, 1229, 539, 716, 419, 729, 273, 163, 208]
    ]
}
```

#### 前端显示

接着，需要在模板文件中定义div和相应的script，为了保证命名统一，统一使用了`url_for`用于命名，该函数可以查找某一函数定义的路由，例如`url_for("month_vnum_vseverity_chart")`返回的就是`main.route`中定义的`/month_vnum_vseverity_chart`

定义div，使用`[1:]`去掉`/`，大小上统一使用`style="width: 100%;height: 300%;"`，尽量不要修改

```html
<div id="{{ url_for("main.month_vnum_vseverity_chart")[1:] }}" style="width: 100%;height: 300%;"></div>
```

定义script

```javascript
function MonthVnumVseverityChart() {
	// 使用main.month_vnum_vseverity_chart是因为该函数定义在main蓝图中
    $.getJSON("{{ url_for("main.month_vnum_vseverity_chart") }}", function(res) {
        // 生成直方图所需的option，如需要使用折线图请将bar换为line
        var option = initDefaultChart("bar", "按月份和漏洞等级统计", res.legend, res.x, res.y);
        // 获得chart的dom实例
        var chart = echarts.init($('#{{ url_for("main.month_vnum_vseverity_chart")[1:] }}')[0]);
        // 设置option，绘制图表
        chart.setOption(option);
    });
}
```

在ChartRender.js中已经定义了2个用于生成option的函数，一般需要已经足够使用，尽量不要修改

```javascript
function initDefaultChart(chartMode, chartTitle, legendData, xAxisData, yAxisData) {
    /**
     * 生成直方图或折线图option
     * @param {string} chartMode - 图表类型，bar表示直方图，line表示折线图
     * @param {string} chartTitle - 标题
     * @param {array} legendData - 图例
     * @param {array} xAxisData - x轴数据
     * @param {array} yAxisData - y轴数据
     * @return {object} option 图表格式
     */
}

function initDefaultPie(chartTitle, legendData, seriesData) {
    /**
     * 生成默认饼图option
     * @param {string} chartTitle - 标题
     * @param {array} legendData - 图例
     * @param {array} seriesData - 数据
     * @return {object} option 图表格式
     */
}
```





