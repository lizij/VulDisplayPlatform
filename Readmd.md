# 移动智能终端漏洞数据监控平台

## 使用

安装依赖库

```shell
pip install -r requirements.txt
```

启动后台服务

```shell
python3 app.py
```

打开浏览器访问`http://127.0.0.1:8080`，可以在`app.py`中修改默认端口

## 设计

本平台使用flask架构作为web服务端，提供后台数据处理和前端界面渲染功能，支持厂商注册和登陆查看

详细设计请参见[设计文档](docs/design.md)